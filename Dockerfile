# Step 1: build binary
FROM docker.io/library/alpine:3.17.2 AS build
RUN apk update --no-progress --quiet && apk upgrade --no-progress --quiet && apk add --no-progress --quiet --no-cache go gcc g++ wget unzip
RUN wget --quiet https://github.com/IBM/plex/releases/download/v6.2.0/TrueType.zip
RUN unzip -qq TrueType.zip
WORKDIR /app
COPY go.mod ./
COPY go.sum ./
RUN go mod download
COPY *.go ./
RUN CGO_ENABLED=1 GOOS=linux go build

# Step 2: deployment image
FROM docker.io/library/alpine:3.17.2
RUN apk update --no-progress --quiet && apk upgrade --no-progress --quiet && apk add --no-progress --quiet --no-cache fortune imagemagick
WORKDIR /app
RUN adduser -D user
RUN mkdir -p /home/user/.local/share/fonts
COPY --from=build /app/imageapi /app/imageapi
COPY --chown=user --from=build /TrueType/IBM-Plex-Mono/IBMPlexMono-Regular.ttf /home/user/.local/share/fonts/IBM-Plex-Mono/IBMPlexMono-Regular.ttf
COPY scripts /app/scripts

EXPOSE 8080
USER user
ENV HOME /home/user

ENTRYPOINT ["/app/imageapi"]
