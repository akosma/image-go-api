package main

import (
	"bytes"
	"context"
	"crypto/md5"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

const BUCKET_NAME = "images"

// Entry point of the program
func main() {
	// Read all environment variables
	endpoint := os.Getenv("MINIO_ENDPOINT")
	publicEndpoint := os.Getenv("MINIO_PUBLIC_ENDPOINT")
	accessKeyID := os.Getenv("MINIO_ACCESS_KEY_ID")
	secretAccessKey := os.Getenv("MINIO_SECRET_ACCESS_KEY")
	useSSL := false
	useSSLEnv, found := os.LookupEnv("MINIO_USE_SSL")
	if found {
		parsed, err := strconv.ParseBool(useSSLEnv)
		if err == nil {
			useSSL = parsed
		} else {
			useSSL = false
		}
	}

	// Initialize connection to Minio at startup
	ctx, minioClient := getMinioClient(endpoint, accessKeyID, secretAccessKey, useSSL)
	createBucket(ctx, minioClient, BUCKET_NAME)
	setBucketPublic(ctx, minioClient, BUCKET_NAME)

	// Router definitions
	r := gin.Default()
	r.GET("/caption", func(c *gin.Context) {
		caption := strings.TrimSpace(c.DefaultQuery("caption", "Your text here"))
		processImageWithCaption(ctx, minioClient, c, caption, useSSL, publicEndpoint)
	})

	r.GET("/fortune", func(c *gin.Context) {
		caption := strings.TrimSpace(fortune())
		processImageWithCaption(ctx, minioClient, c, caption, useSSL, publicEndpoint)
	})

	r.GET("/random", func(c *gin.Context) {
		width, w := validateIntegerValue(c.DefaultQuery("width", "400"))
		height, h := validateIntegerValue(c.DefaultQuery("height", "300"))

		f, err := os.CreateTemp("", "*-random.png")
		if err != nil {
			log.Println("Could not create temporary file with random filename")
			log.Fatalln(err)
		}
		tempFilePath := f.Name()
		filename := filepath.Base(tempFilePath)
		log.Println("Generating random image " + tempFilePath)
		createRandomImage(width, height, tempFilePath)
		uploadObject(ctx, minioClient, filename, tempFilePath, c, endpoint, w, h, useSSL)
	})

	// This application is ready when it can talk to Minio
	r.GET("/healthz", func(c *gin.Context) {
		probe(ctx, minioClient, BUCKET_NAME, c)
	})
	r.Run()
}

// This check is performed by the liveness and readiness probe on OpenShift.
func probe(ctx context.Context, minioClient *minio.Client, bucketName string, c *gin.Context) {
	exists, err := minioClient.BucketExists(ctx, bucketName)
	if err == nil && exists {
		c.JSON(200, gin.H{
			"ready": "true",
		})
	} else {
		c.JSON(500, gin.H{
			"ready": "false",
		})
	}
}

// Takes a string caption, a width and a height, and verifies that such
// image was not previously stored on the Minio storage. If that's the case,
// it returns a JSON object with the URL of the file.
// Otherwise, it uses ImageMagick to create the image, it uploads it to
// Minio, and sends the same JSON object to the client.
func processImageWithCaption(ctx context.Context, minioClient *minio.Client, c *gin.Context, caption string, useSSL bool, endpoint string) {
	width, w := validateIntegerValue(c.DefaultQuery("width", "400"))
	height, h := validateIntegerValue(c.DefaultQuery("height", "300"))
	filename := md5Encode(width+"_"+height+"_"+caption) + ".png"

	info, err := getObjectInfo(ctx, minioClient, filename)
	if err == nil {
		log.Println("Object exists: send URL to client")
		sendJSON(c, endpoint, filename, w, h, info.Size, useSSL)
	} else {
		log.Println("Object does not exist: create, upload, and send URL to client")
		tempFile := "/tmp/" + filename
		createImageWithCaption(width, height, caption, tempFile)
		uploadObject(ctx, minioClient, filename, tempFile, c, endpoint, w, h, useSSL)
	}
}

// Wraps the Minio API to upload a file.
func uploadObject(ctx context.Context, minioClient *minio.Client, filename string, tempFile string, c *gin.Context, endpoint string, w int, h int, useSSL bool) {
	info, err := minioClient.FPutObject(ctx, BUCKET_NAME, filename, tempFile, minio.PutObjectOptions{ContentType: "image/png"})
	if err != nil {
		c.JSON(500, gin.H{
			"error": "Could not upload file to Minio",
		})
		log.Println("Could not upload file to Minio")
		log.Fatalln(err)
	}

	log.Printf("Successfully uploaded %s of size %d\n", filename, info.Size)
	sendJSON(c, endpoint, filename, w, h, info.Size, useSSL)
	os.Remove(tempFile)
}

// Wraps the Minio API call to get information about a specific object.
func getObjectInfo(ctx context.Context, minioClient *minio.Client, filename string) (minio.ObjectInfo, error) {
	info, err := minioClient.StatObject(ctx, BUCKET_NAME, filename, minio.StatObjectOptions{})
	return info, err
}

// Generates and sends a JSON 200 response to the client.
func sendJSON(c *gin.Context, endpoint string, filename string, width int, height int, size int64, useSSL bool) {
	protocol := "http"
	if useSSL {
		protocol = "https"
	}
	url := fmt.Sprintf("%s://%s/%s/%s", protocol, endpoint, BUCKET_NAME, filename)

	c.JSON(200, gin.H{
		"endpoint": endpoint,
		"protocol": protocol,
		"url":      url,
		"width":    width,
		"height":   height,
		"filename": filename,
		"bucket":   BUCKET_NAME,
		"size":     size,
	})
}

// Called at application startup, creates a connection to the Minio storage.
func getMinioClient(endpoint string, accessKeyID string, secretAccessKey string, useSSL bool) (context.Context, *minio.Client) {
	ctx := context.Background()

	minioClient, err := minio.New(endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(accessKeyID, secretAccessKey, ""),
		Secure: useSSL,
	})
	if err != nil {
		log.Println("Could not connect to Minio")
		log.Fatalln(err)
	}
	return ctx, minioClient
}

// Wraps the Minio API to check if a bucket exists and it creates it if not.
func createBucket(ctx context.Context, minioClient *minio.Client, bucketName string) {
	exists, err := minioClient.BucketExists(ctx, bucketName)
	if err == nil && exists {
		log.Printf("Bucket '%s' exists\n", bucketName)
	} else {
		log.Printf("Bucket '%s' does not exist, creating\n", bucketName)
		err := minioClient.MakeBucket(ctx, bucketName, minio.MakeBucketOptions{})
		if err == nil {
			log.Printf("Successfully created '%s'\n", bucketName)
		} else {
			log.Println("Error while creating the bucket")
			log.Fatalln(err)
		}
	}
}

// Changes the policy of the specified bucket to allow download of individual items
// but not the listing of all objects on the bucket.
func setBucketPublic(ctx context.Context, minioClient *minio.Client, bucketName string) {
	policy := fmt.Sprintf(`{
			"Statement": [{
				"Action": ["s3:GetBucketLocation"],
				"Effect": "Allow",
				"Principal": {
					"AWS": ["*"]
				},
				"Resource": ["arn:aws:s3:::%s"]
			}, {
				"Action": ["s3:GetObject"],
				"Effect": "Allow",
				"Principal": {
					"AWS": ["*"]
				},
				"Resource": ["arn:aws:s3:::%s/*"]
			}],
			"Version": "2012-10-17"
		}`, bucketName, bucketName)
	minioClient.SetBucketPolicy(ctx, bucketName, policy)
}

// Executes the `fortune` application and returns the generated text.
func fortune() string {
	cmd := exec.Command("fortune")
	cmd.Stdin = strings.NewReader("")

	var out bytes.Buffer
	cmd.Stdout = &out

	err := cmd.Run()

	if err != nil {
		log.Println("Could not run the fortune application")
		log.Fatal(err)
	}

	return out.String()
}

// Wraps ImageMagick to generate an image with a caption. Check the specified
// `scripts/caption.sh` file to see how this is done.
func createImageWithCaption(width string, height string, caption string, output string) {
	cmd := exec.Command("scripts/caption.sh", width, height, caption, output)
	err := cmd.Run()

	if err != nil {
		log.Println("Could not create image with caption")
		log.Fatal(err)
	}
}

// Wraps ImageMagick to generate an image with random colors. Check the
// specified `scripts/random.sh` file to see how this is done.
func createRandomImage(width string, height string, output string) {
	cmd := exec.Command("scripts/random.sh", width, height, output)
	err := cmd.Run()

	if err != nil {
		log.Println("Could not create random image")
		log.Fatal(err)
	}
}

// Encodes the string passed as input with the MD5 hashing algorithm.
func md5Encode(input string) string {
	data := []byte(input)
	return fmt.Sprintf("%x", md5.Sum(data))
}

// Validates the integer values passed as parameter by the user.
// Images should have between 10 and 2000 pixels in width and/or height.
func validateIntegerValue(value string) (string, int) {
	defaultValueString := "100"
	defaultValue := 100
	i, err := strconv.Atoi(value)
	if err != nil {
		log.Printf("Invalid value '%s'\n", value)
		return defaultValueString, defaultValue
	}
	if i < 10 || i > 2000 {
		log.Printf("Invalid value '%s'\n", value)
		return defaultValueString, defaultValue
	}
	return value, i
}
