#!/usr/bin/env sh

FONT=$HOME/.local/share/fonts/IBM-Plex-Mono/IBMPlexMono-Regular.ttf

magick -gravity center -background black -fill white -size "$1"x"$2" -font "$FONT" caption:"$3" "$4"
