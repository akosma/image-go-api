#!/usr/bin/env sh

magick convert -size "$1"x"$2" plasma:fractal "$3"
